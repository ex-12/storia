const numGenerato = Math.floor(Math.random() * 20) + 1;
let numTentativi = 0;
function controllaNumero() {
  const numUtente = Number(document.getElementById("numero").value);
  numTentativi++;
  if (numUtente === numGenerato) {
    alert(`Complimenti! Hai indovinato il numero in ${numTentativi} tentativi.`);
  } else if (numUtente > numGenerato) {
    alert("Troppo grande. Prova di nuovo.");
  } else {
    alert("Troppo piccolo. Prova di nuovo.");
  }
}
document.getElementById("controlla").addEventListener("click", controllaNumero);